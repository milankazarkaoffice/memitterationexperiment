//
//  pqtestTests.m
//  pqtestTests
//
//  Created by Milan Kazarka on 4/20/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "CAlerts.h"
#import "CCoding.h"
#import "OCCodingWrapper.h"

@interface pqtestTests : XCTestCase

@end

@implementation pqtestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    /**
     
     the idea (which is an oversimplification) is that you'd have only one Alert view of a speciffic type shown on screen at any given time.
     
     */
    
    CAlert *ca01 = [[CAlert alloc] initWithType:_CALERT_TYPE_WARNING speciffic:_CALERT_TYPE_SELECTION selection:nil];
    BOOL pass01 = [[CAlerts shared] addAlert:ca01]; // must be True
    BOOL pass02 = [[CAlerts shared] addAlert:ca01]; // must be False - we can't add the same alert twice
    [ca01 show];
    CAlert *ca02 = [[CAlert alloc] initWithType:_CALERT_TYPE_WARNING speciffic:_CALERT_TYPE_SELECTION selection:nil];
    BOOL pass03 = [[CAlerts shared] addAlert:ca02]; // must be False, since we're already showing one warning of this type
    
    XCTAssertTrue(pass01,@"adding alert holders fails");
    XCTAssertFalse(pass02,@"adding the same alert holder doesn't fail");
    XCTAssertFalse(pass03,@"adding the same alert type doesn't fail");
    
    // the results of the actual selections on the various AVs are stored in the CAlert objects - one more test would be to retrieve that (that's a todo)
    
    // coding tests
    
    long timestamp = 0;
    unsigned char pid[2];
    unsigned char somedata[_SDATA_MAX];
    
    initRunner();
    initPool((unsigned char*)"hello world and beyond", 22, &timestamp, (unsigned char*)pid);
    struct _SDATA_POOL *pool = poolAtID((unsigned char*)pid, 1);
    
    XCTAssertNotEqual(pool,NULL,@"retrieving our data handler didn't work");
    
    sleep(1);
    
    lockRunner();
    dataPoolMoveBackward(pool, pool->lastTimestamp-timestamp);
    timestamp = pool->lastTimestamp;
    printf("    RECONSTRUCTION\n");
    showCurrentReconstruction(pool);
    copyCurrentReconstruction(pool, (unsigned char*)somedata);
    removePoolAtId((unsigned char*)pid, 0);
    unlockRunner();
    
    int diff = strcmp((const char*)somedata,"hello world and beyond");
    
    XCTAssertEqual(diff,0,@"decoding didn't work");
    
    // we test the OC wrapper
    
    NSMutableString *mutable = [[NSMutableString alloc] init];
    [[OCCodingWrapper shared] storeData:@"hello" timestamp:&timestamp pid:(unsigned char*)pid];
    [[OCCodingWrapper shared] retrieveData:mutable timestamp:&timestamp pid:(unsigned char*)pid];
    
    XCTAssertTrue([mutable isEqualToString:@"hello"],@"wrapper is broken");
    
    NSLog(@"finished with concatenated tests");
}

//long timestamp = 0;
//unsigned char pid[2];

- (void)testPerformanceExample {
    
    // todo - performance test on this might be interesting
    /**
    initRunner();
    initPool((unsigned char*)"hello world and beyond", 22, &timestamp, (unsigned char*)pid);
    struct _SDATA_POOL *pool = poolAtID((unsigned char*)pid, 1);
    
    sleep(3);
    */
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
        /**
        lockRunner();
        dataPoolMoveBackward(pool, pool->lastTimestamp-timestamp);
        timestamp = pool->lastTimestamp;
        printf("    RECONSTRUCTION\n");
        showCurrentReconstruction(pool);
        removePoolAtId((unsigned char*)pid, 0);
        unlockRunner();
        */
    }];
}

@end
