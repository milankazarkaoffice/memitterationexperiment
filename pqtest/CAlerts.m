//
//  CAlerts.m
//  pqtest
//
//  Created by Milan Kazarka on 4/21/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import "CAlerts.h"

@implementation CAlerts

static CAlerts *CAlertsInstance = nil;

+(id)shared
{
    if (!CAlertsInstance)
        CAlertsInstance = [[CAlerts alloc] init];
    return CAlertsInstance;
}

-(id)init
{
    if (CAlertsInstance)
        return CAlertsInstance;
    self = [super init];
    if (self)
    {
        CAlertsInstance = self;
        self.alerts = [[NSMutableArray alloc] init];
        self.predefMessages = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(CAlert*)activeAlertForSpecifficType:(NSInteger)specifficType
{
    @synchronized (self) {
        for(int n = 0; n < [self.alerts count]; n++)
        {
            CAlert *current = [self.alerts objectAtIndex:n];
            if (current.specifficType==specifficType)
            {
                return current;
            }
        }
    }
    
    return NULL;
}

-(BOOL)addAlert:(CAlert*)alert
{
    if (!alert)
        return NO;
    
    @synchronized (self) {
        for(int n = 0; n < [self.alerts count]; n++)
        {
            CAlert *current = [self.alerts objectAtIndex:n];
            if (current==alert)
            {
                // warning
                return NO;
            }
        }
        
        // this won't happen if we're showing the alert view triggered by a UI action - since the screen is locked if we're already showing an alert view, but if the AV is triggered / shown through a non-UI event than this can happen
        
        CAlert *alreadyShowing = [self activeAlertForSpecifficType:alert.specifficType];
        if (alreadyShowing)
        {
            // attach / process the currently showing AV
            
            // in some cases we might want to modify the contents of an AlertView / switch it / do something else in case that we already have an AV of the same speciffic type shown
            
            return NO;
        }
        
        // experiment, not yet know how this would be used in real life, but we preset a message for all AV types
        
        alert.message = [self.predefMessages objectForKey:[NSNumber numberWithInteger:alert.specifficType]];
        [alert createAv];
        [alert show];
        
        alert.delegate = self;
        [self.alerts addObject:alert];
    }
    return YES;
}

-(void)onStateChange:(CAlert*)calert
{
    NSLog(@"CAlerts onStateChange");
    // something useful
}

-(BOOL)setMessageForSpecifficType:(NSInteger)specifficType message:(NSString*)message
{
    if (!message)
        return NO;
    
    [self.predefMessages setObject:message forKey:[NSNumber numberWithInteger:specifficType]];
    
    return YES;
}

@end
