//
//  CCoding.c
//  pqtest
//
//  Created by Milan Kazarka on 4/20/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#include "CCoding.h"

pthread_mutex_t gpoolLock;
pthread_t gpoolThread;

void lockRunner( );
void unlockRunner( );

struct _SDATA_POOL *pools = NULL;

// testing purposes:
//long int test_timestamp = 0;

long int milliseconds( )
{
    struct timeval tp;
    gettimeofday(&tp,NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    return ms;
}

unsigned char *randomMalloc( int len )
{
    unsigned char *rd = malloc(len);
    for (int n = 0; n < len; n++) {
         rd[n] = (unsigned char) (rand() % 255 + 1);
    }
    return rd;
}

void *runner(void*none)
{
    while(1)
    {
        printf("runner(%ld)\n",milliseconds());
        usleep(rand() % 100000);
        lockRunner();
        struct _SDATA_POOL *current = pools;
        while(current)
        {
            dataPoolOnClocktick(current);
            showCurrentReconstruction(current);
            current = current->next;
        }
        unlockRunner();
    }
    return NULL; // never reached
}

void initRunner( )
{
    static int run = 0;
    if (run)
        return;
    pthread_mutex_init(&gpoolLock,NULL);
    pthread_create(&gpoolThread, NULL, runner, NULL);
    run++;
}

void lockRunner( )
{
    pthread_mutex_lock(&gpoolLock);
}

void unlockRunner( )
{
    pthread_mutex_unlock(&gpoolLock);
}

struct _SDATA_CHUNK *dataPoolGetLast( struct _SDATA_POOL *pool )
{
    struct _SDATA_CHUNK *current = pool->chunks;
    while(current)
    {
        if (!current->next)
            return current;
        current = current->next;
    }
    return NULL;
}

void dataChunkMoveForward( struct _SDATA_CHUNK *chunk, long int amount )
{
    unsigned char c = chunk->data[0];
    
    for(long int n = 0; n < amount; n++)
    {
        if (c!=0xff)
            c += 0x01;
        else
            c = 0x00;
    }
    
    chunk->data[0]=c;
}

void dataChunkMoveBackward( struct _SDATA_CHUNK *chunk, long int amount )
{
    unsigned char c = chunk->data[0];
    
    for(long int n = amount; n > 0; n--)
    {
        if (c!=0x00)
            c -= 0x01;
        else
            c = 0xff;
    }
    
    chunk->data[0]=c;
}

void dataPoolMoveForward( struct _SDATA_POOL *pool, long int amount )
{
    if (!pool)
        return;
    
    struct _SDATA_CHUNK *current = pool->chunks;
    while(current)
    {
        dataChunkMoveForward(current, amount);
        current = current->next;
    }
    
    struct _SDATA_CHUNK *last = dataPoolGetLast(pool);
    current = pool->chunks;
    struct _SDATA_CHUNK *prev = NULL;
    for(long int n = 0; n < amount+1; n++)
    {
        struct _SDATA_CHUNK *next = current->next;
        if (!next)
        {
            next = pool->chunks;
        }
        
        if (n==amount)
        {
            if (current==pool->chunks) // we ended up at the same position
                return;
            
            last->next = pool->chunks;
            pool->chunks->prev = last;
            pool->chunks = current;
            current->prev = NULL;
            current->next = next;
            if (prev)
                prev->next = NULL;
            
        }
        
        prev = current;
        current = next;
    }

}

void dataPoolMoveBackward( struct _SDATA_POOL *pool, long int amount )
{
    if (!pool)
        return;
    
    struct _SDATA_CHUNK *current = pool->chunks;
    while(current)
    {
        dataChunkMoveBackward(current, amount);
        current = current->next;
    }
    
    struct _SDATA_CHUNK *last = dataPoolGetLast(pool);
    current = pool->chunks;
    struct _SDATA_CHUNK *prev = NULL;
    for(long int n = amount; n > -1; n--)
    {
        prev = current->prev;
        if (!prev)
            prev = last;
        
        struct _SDATA_CHUNK *next = current->next;
        if (!next)
            next = pool->chunks;
        
        if (n==0)
        {
            if (current==pool->chunks) // we ended up at the same position
                return;
            
            last->next = pool->chunks;
            pool->chunks->prev = last;
            pool->chunks = current;
            current->prev = NULL;
            current->next = next;
            if (prev)
                prev->next = NULL;
            
        }
        current = prev;
    }
    
    // the original timestamp we're holding needs to be reset to the newest value after each move backwards
}

void dataPoolOnClocktick( struct _SDATA_POOL *pool )
{
    if (!pool)
        return;
    
    if (!pool->chunks->next)
        return;
        
    long int newTimestamp = milliseconds();
    long int diff = newTimestamp-pool->lastTimestamp; // the diff is what we work with
    
    if (diff<2)
        return; // todo
    
    pool->lastTimestamp = newTimestamp;
    
    printf("    pool(%p) diff(%ld)\n",pool,diff);
    
    dataPoolMoveForward(pool, diff);
    //showCurrentReconstruction(pool);
    //dataPoolMoveBackward(pool, diff); // test
    //showCurrentReconstruction(pool);
    //printf("    moving(%ld) sum(%ld) test(%ld) current(%ld)\n",newTimestamp-test_timestamp,sum,test_timestamp,newTimestamp);
}

struct _SDATA_POOL *poolAtID( unsigned char *id, int sync )
{
    if (!id)
        return NULL;
    if (sync==1)
        lockRunner();
    
    struct _SDATA_POOL *current = pools;
    while(current)
    {
        if (current->id[0]==id[0] && current->id[1]==id[1])
        {
            if (sync==1)
                unlockRunner();
            return current;
        }
        current = current->next;
    }
    
    if (sync==1)
        unlockRunner();
    
    return NULL;
}

void removePoolData( struct _SDATA_POOL *pool )
{
    if (!pool)
        return;
    
    pool->id[0] = (unsigned char) (rand() % 255 + 1);
    pool->id[1] = (unsigned char) (rand() % 255 + 1);
    
    struct _SDATA_CHUNK *current = pool->chunks;
    while(current)
    {
        struct _SDATA_CHUNK *next = current->next;
        current->data[0] = (unsigned char) (rand() % 255 + 1);
        current->data[1] = (unsigned char) (rand() % 255 + 1);
        free(current);
        current = next;
    }
    
    free(pool);
}

void removePoolAtId( unsigned char *id, int sync )
{
    if (!id)
        return;
    if (sync==1)
        lockRunner();
    
    struct _SDATA_POOL *prev = NULL;
    struct _SDATA_POOL *current = pools;
    while(current)
    {
        if (current->id[0]==id[0] && current->id[1]==id[1])
        {
            if (!prev)
                pools = current->next;
            else
                prev->next = current->next;
            
            removePoolData(current);
            break;
        }
        prev = current;
        current = current->next;
    }
    
    if (sync==1)
        unlockRunner();
}

void poolAddChunk( struct _SDATA_POOL *pool, struct _SDATA_CHUNK *chunk )
{
    if (!pool || !chunk)
        return;
    
    chunk->next = NULL;
    chunk->prev = NULL;
    
    struct _SDATA_CHUNK *current = pool->chunks;
    if (!current)
    {
        pool->chunks = chunk;
        return;
    }
    else
    {
        struct _SDATA_CHUNK *prev = NULL;
        while(current)
        {
            if (!current->next)
            {
                chunk->prev = current;
                current->next = chunk;
                return;
            }
            prev = current;
            current = current->next;
        }
    }
}

void copyCurrentReconstruction( struct _SDATA_POOL *pool, unsigned char *reconstruction )
{
    if (!pool)
        return;
    int n = 0;
    struct _SDATA_CHUNK *current = pool->chunks;
    while(current)
    {
        reconstruction[n] = current->data[0];
        current = current->next;
        n++;
    }
    reconstruction[n] = 0x00;
}

void showCurrentReconstruction( struct _SDATA_POOL *pool )
{
    if (!pool)
        return;
    unsigned char reconstruction[_SDATA_MAX];
    int n = 0;
    struct _SDATA_CHUNK *current = pool->chunks;
    while(current)
    {
        reconstruction[n] = current->data[0];
        current = current->next;
        n++;
    }
    reconstruction[n] = 0x00;
    printf("    rec(%s)\n",(char*)reconstruction);
    memset((void*)reconstruction,0,_SDATA_MAX);
}

int initPool( unsigned char *data, int len, long int *timestamp, unsigned char *pid )
{
    if (len>_SDATA_MAX)
        return 0;
    
    *timestamp = milliseconds();
    //test_timestamp = *timestamp;
    
    struct _SDATA_POOL *pool = (struct _SDATA_POOL*)malloc(sizeof(struct _SDATA_POOL));
    if (!pool)
    {
        // well, in reality this shouldn't happen
        return 0;
    }
    
    pool->next = NULL;
    pool->lastTimestamp = *timestamp;
    pool->chunks = NULL;
    
    lockRunner();
    unsigned char current[2] = {0x00, 0x00};
    while(1)
    {
        current[0] = (unsigned char) (rand() % 255 + 1);
        current[1] = (unsigned char) (rand() % 255 + 1);
        if (!poolAtID((unsigned char*)current,0))
            break;
    }
    pool->id[0] = pid[0] = current[0];
    pool->id[1] = pid[1] = current[1];
    current[0] = 0x00;
    current[1] = 0x00;
    
    if (!pools)
        pools = pool;
    else
    {
        struct _SDATA_POOL *current = pools;
        while(current)
        {
            if (!current->next)
            {
                current->next = pool;
                break;
            }
            current = current->next;
        }
    }
    
    usleep(rand() % 1000);
    
    for(int n = 0; n < len; n++)
    {
        struct _SDATA_CHUNK *chunk = (struct _SDATA_CHUNK*)malloc(sizeof(struct _SDATA_CHUNK));
        chunk->data[0] = data[n];
        poolAddChunk(pool,chunk);
    }
    printf("    pool setup end\n");
    showCurrentReconstruction(pool);
    unlockRunner();
    
    return 1;
}
