//
//  OCCodingWrapper.h
//  pqtest
//
//  Created by Milan Kazarka on 4/21/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "CCoding.h"

// I ended up accessing the C functions from code, only using this in the tests
// in real life we'd do a Swift wrapper

@interface OCCodingWrapper : NSObject

+(id)shared;

-(BOOL)storeData:(NSString*)string timestamp:(long*)timestamp pid:(unsigned char*)pid;
-(BOOL)retrieveData:(NSMutableString*)mutable timestamp:(long*)timestamp pid:(unsigned char*)pid;

@end
