//
//  OCCodingWrapper.m
//  pqtest
//
//  Created by Milan Kazarka on 4/21/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import "OCCodingWrapper.h"

@implementation OCCodingWrapper

static OCCodingWrapper *OCCodingWrapperInstance = nil;

+(id)shared
{
    if (!OCCodingWrapperInstance)
        OCCodingWrapperInstance = [[OCCodingWrapper alloc] init];
    return OCCodingWrapperInstance;
}

-(id)init
{
    if (OCCodingWrapperInstance)
        return OCCodingWrapperInstance;
    
    self = [super init];
    if (self)
    {
        initRunner();
    }
    return self;
}

-(BOOL)storeData:(NSString*)string timestamp:(long*)timestamp pid:(unsigned char*)pid
{
    if (!string || !pid || !timestamp)
    {
        // todo, reasonable message
        return NO;
    }
    
    if (!initPool((unsigned char*)string.UTF8String, (int)[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding], timestamp, (unsigned char*)pid))
        return NO;
    
    return YES;
}

-(BOOL)retrieveData:(NSMutableString*)mutable timestamp:(long*)timestamp pid:(unsigned char*)pid
{
    unsigned char somedata[_SDATA_MAX];
    struct _SDATA_POOL *pool = poolAtID(pid, 1);
    
    lockRunner();
    dataPoolMoveBackward(pool, pool->lastTimestamp-*timestamp);
    *timestamp = pool->lastTimestamp;
    printf("    RECONSTRUCTION\n");
    showCurrentReconstruction(pool);
    copyCurrentReconstruction(pool, (unsigned char*)somedata);
    removePoolAtId((unsigned char*)pid, 0);
    unlockRunner();
    [mutable setString:[NSString stringWithUTF8String:(char*)somedata]];
    memset((void*)somedata,0,_SDATA_MAX);
    
    return YES;
}

@end
