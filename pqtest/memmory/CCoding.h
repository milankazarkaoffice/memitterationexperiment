//
//  CCoding.h
//  pqtest
//
//  Created by Milan Kazarka on 4/20/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

/**
 
 this is just an idea:
 
    I store sensitive data in chunks and these chunks are in a linked list. The positions of the elements in that list are moved in one direction at each clocktick initiated in a separate thread. No structure here knows the original timestamp, we should know that from where we need to use the data. 
 
    The idea is that the stored data here changes by time and we pull it out / decrypt it based on the timestamp we remember and the timestamp of the last move of the items in the linked list.
 
    So, this is just an idea. In no way proven or anything. It's an excersize in how data would be continuously changed in memmory and than decrypted by moving backwards in time, since we only store the correct timestamp elsewhere. It needs much more work.
 
    This is inefficient in this form in that the longer you store data in memmory the more cycles / processing power would it take to wind it back to the decrypted form.
 
 */

#ifndef CCoding_h
#define CCoding_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <pthread.h>

#define _SDATA_MAX 1024

struct _SDATA_CHUNK {
    unsigned char data[2]; // todo - the idea is to save the one char as 2 after coding
    int len;
    struct _SDATA_CHUNK *prev;
    struct _SDATA_CHUNK *next;
};

struct _SDATA_POOL {
    unsigned char id[2]; // this doesn't expect many pools to exist
    unsigned char *memmory;
    struct _SDATA_CHUNK *chunks;
    struct _SDATA_POOL *next;
    long int lastTimestamp; // we store the "last", not nessesarily the "first" timestamp
};

long int milliseconds( );
void dataPoolOnClocktick( struct _SDATA_POOL *pool );
int initPool( unsigned char *data, int len, long int *timestamp, unsigned char *pid );
unsigned char *randomMalloc( int len );
struct _SDATA_POOL *poolAtID( unsigned char *id, int sync );
void removePoolAtId( unsigned char *id, int sync );
void showCurrentReconstruction( struct _SDATA_POOL *pool );
void copyCurrentReconstruction( struct _SDATA_POOL *pool, unsigned char *reconstruction );
void dataPoolMoveBackward( struct _SDATA_POOL *pool, long int amount );
void lockRunner( );
void unlockRunner( );

void initRunner( );

#endif /* CCoding_h */
