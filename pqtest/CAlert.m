//
//  CAlert.m
//  pqtest
//
//  Created by Milan Kazarka on 4/21/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import "CAlert.h"

@implementation CAlert

-(id)initWithType:(NSInteger)type speciffic:(NSInteger)specifficType selection:(NSMutableArray*)selection
{
    self = [super init];
    if (self)
    {
        self.resultOfSelection = -1;
        self.delegate = NULL;
        self.specifficType = specifficType;
        self.type = type;
        self.message = nil;
    }
    return self;
}

-(void)createAv
{
    if (self.type==_CALERT_TYPE_WARNING)
    {
        self.av = [[UIAlertView alloc]initWithTitle:@"Oh my" message:self.message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    }
    else if (self.type==_CALERT_TYPE_SELECTION)
    {
        // todo
    }
}

-(void)show
{
    [self.av show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"CAlert clickedButtonAtIndex(%ld)",buttonIndex);
    
    self.resultOfSelection = buttonIndex;
    
    if (self.delegate)
    {
        [self.delegate onStateChange:self];
    }
}

@end
