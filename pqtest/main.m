//
//  main.m
//  pqtest
//
//  Created by Milan Kazarka on 4/20/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#include "CCoding.h"

int main(int argc, char * argv[]) {
    
    initRunner();
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
