//
//  CAlerts.h
//  pqtest
//
//  Created by Milan Kazarka on 4/21/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import "CAlert.h"

@interface CAlerts : NSObject <CAlertDelegate>

// for the sake of it, I just fill this list & hold alerts and the results of the selections until we see fit

@property NSMutableArray *alerts;
@property NSMutableDictionary *predefMessages;

+(id)shared;
-(BOOL)addAlert:(CAlert*)alert;
-(CAlert*)activeAlertForSpecifficType:(NSInteger)specifficType;

// a test to show that this works too, though not really practical
-(BOOL)setMessageForSpecifficType:(NSInteger)specifficType message:(NSString*)message;

@end
