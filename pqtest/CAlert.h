//
//  CAlert.h
//  pqtest
//
//  Created by Milan Kazarka on 4/21/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

enum _CALERT_TYPES {
    _CALERT_TYPE_GENERIC = 100,
    _CALERT_TYPE_WARNING = 101,
    _CALERT_TYPE_SELECTION = 102
};

enum _CALERT_SPECIFFICTYPE {
    _CALERT_SPECIFFICTYPE_GENERIC = 100,
    _CALERT_SPECIFFICTYPE_NODATAINENTRY = 101
};

@protocol CAlertDelegate <NSObject>
-(void)onStateChange:(id)calert;
@end

@interface CAlert : NSObject <UIAlertViewDelegate>

@property NSInteger specifficType;
@property NSInteger type;
@property BOOL active;
@property UIAlertView *av;
@property NSObject<CAlertDelegate> *delegate;
@property NSString *message;

@property NSInteger resultOfSelection;

-(id)initWithType:(NSInteger)type speciffic:(NSInteger)specifficType selection:(NSMutableArray*)selection;
-(void)createAv;
-(void)show;

@end
