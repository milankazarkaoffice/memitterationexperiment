//
//  ViewController.h
//  pqtest
//
//  Created by Milan Kazarka on 4/20/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(IBAction)onStore:(id)sender;

@property IBOutlet UITextField *entry;
@property IBOutlet UILabel *reconstructionLbl;
@property IBOutlet UIButton *store;

@end

