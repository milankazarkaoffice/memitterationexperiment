//
//  ViewController.m
//  pqtest
//
//  Created by Milan Kazarka on 4/20/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

#import "ViewController.h"
#include "CCoding.h"
#import "CAlerts.h"

@interface ViewController () {
@public
    long timestamp;
    unsigned char pid[2];
}

@property BOOL storing;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.storing = NO;
    [NSTimer scheduledTimerWithTimeInterval:0.5f
                                     target:self selector:@selector(reconstructorCall) userInfo:nil repeats:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reconstructorCall
{
    @synchronized (self) {
        if (self.storing)
        {
            // this just serves to see that the data is actually permutating
            // but there is no real practical purpose
                        
            lockRunner();
            dispatch_async(dispatch_get_main_queue(), ^{
            unsigned char somedata[_SDATA_MAX];
            unsigned char readable[_SDATA_MAX];
            unsigned char charVis[20]; // 20 is just a bogus number, we show what we can
            struct _SDATA_POOL *pool = poolAtID((unsigned char*)pid, 0);
            if (pool)
            {
                copyCurrentReconstruction(pool, (unsigned char*)somedata);
                printf("    rec from UI(%s)\n",(char*)somedata);
                
                readable[0] = 0x00;
                for(int n = 0; n < 5; n++)
                {
                    sprintf((char*)charVis,"%x02",somedata[n]);
                    strcat((char*)readable,(char*)charVis);
                }
                
                self.reconstructionLbl.text = [NSString stringWithUTF8String:(char*)readable];
                printf("READABLE(%s)\n",(char*)readable);
                
                memset((void*)somedata,0,_SDATA_MAX);
                memset((void*)readable,0,_SDATA_MAX);
                memset((void*)charVis,0,20);
            }
            });
            unlockRunner();
        }
    }
}

-(IBAction)onStore:(id)sender
{
    if (!self.storing)
    {
        if (self.entry.text.length == 0)
        {
            [[CAlerts shared] setMessageForSpecifficType:_CALERT_TYPE_WARNING message:@"No text to store"];
            
            CAlert *alert = [[CAlert alloc] initWithType:_CALERT_TYPE_WARNING speciffic:_CALERT_SPECIFFICTYPE_NODATAINENTRY selection:nil];
            [[CAlerts shared] addAlert:alert];
            return;
        }
            
        initPool((unsigned char*)self.entry.text.UTF8String, (int)[self.entry.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding], &timestamp, (unsigned char*)pid);
        
        self.storing = YES;
        
        [self.entry resignFirstResponder];
        self.entry.enabled = NO;
        self.entry.text = @"";
        [self.store setTitle:@"Reconstruct" forState:UIControlStateNormal];
    }
    else
    {
        @synchronized (self) {
            
            // we can also go through OCCodingWrapper's interface - I'm using that in the pqtestTests
            
            self.entry.enabled = YES;
            self.storing = NO;
            
            lockRunner();
            
            unsigned char somedata[_SDATA_MAX];
            
            struct _SDATA_POOL *pool = poolAtID((unsigned char*)pid, 0);
            if (pool)
            {
                dataPoolMoveBackward(pool, pool->lastTimestamp-timestamp);
                showCurrentReconstruction(pool);
                copyCurrentReconstruction(pool, (unsigned char*)somedata);
                self.reconstructionLbl.text = [NSString stringWithUTF8String:(char*)somedata];
                removePoolAtId((unsigned char*)pid, 0);
                memset((void*)somedata,0,_SDATA_MAX);
            }

            unlockRunner();
            [self.store setTitle:@"Store" forState:UIControlStateNormal];
        }
    }
}

@end
